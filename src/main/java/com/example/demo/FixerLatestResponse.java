package com.example.demo;

import java.io.Serializable;
import java.util.Map;

public class FixerLatestResponse implements Serializable {
    private Boolean success;

    private Long timestamp;

    private String base;

    private Map<String, Double> curRateMap;

    public FixerLatestResponse(Boolean success, Long timestamp, String base, Map<String, Double> curRateMap) {
        this.success = success;
        this.timestamp = timestamp;
        this.base = base;
        this.curRateMap = curRateMap;
    }


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Map<String, Double> getCurRateMap() {
        return curRateMap;
    }

    public void setCurRateMap(Map<String, Double> curRateMap) {
        this.curRateMap = curRateMap;
    }
}
