package com.example.demo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import java.util.Collection;

public interface RateRepository extends CrudRepository<Rate, Long> {

    @Query("SELECT rate FROM Rate rate WHERE rate.base = ?1 AND rate.target = ?2 AND rate.startDate >= ?3 AND rate.endDate <= ?4")
    Collection<Rate> findAllRatesInDate(String base, String target, Long startDate, Long endDate);
}
