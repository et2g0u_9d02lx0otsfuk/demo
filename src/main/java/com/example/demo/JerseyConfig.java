package com.example.demo;

import com.example.demo.RateSampler;
import com.example.demo.RateService;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(RateService.class);
        register(RateSampler.class);
    }
}