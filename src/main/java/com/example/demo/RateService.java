package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import java.util.Collection;

@Component
@Path("api/v1")
public class RateService {

    private final RateRepository rateRepository;

    @Value("base_currency")
    String baseCurrency;

    @Value("target_currency")
    String targetCurrency;

    private volatile Rate latest;

    @Autowired
    public RateService(RateRepository rateRepository) {
        this.rateRepository = rateRepository;
    }

    @Path("rate")
    @GET
    public Rate getLatestRate() {
        return latest;
    }

    @Path("history")
    @GET
    public Collection<Rate> getRateHistory(@QueryParam("startDate") @NotNull Long startDate,
                                           @QueryParam("endDate") @NotNull Long endDate) {
        return rateRepository.findAllRatesInDate(baseCurrency, targetCurrency, startDate, endDate);
    }

    public void updateRate(Rate rate) {
        this.latest = rate;
        rateRepository.save(rate);
    }
}
