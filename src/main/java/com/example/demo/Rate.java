package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Rate {

    @Id
    public final String base;

    @Id
    public final String target;

    public final Long date;

    public final Double value;

    public Rate(String base, String target, Double value, Long date) {
        this.base = base;
        this.target = target;
        this.value = value;
        this.date = date;
    }
}
