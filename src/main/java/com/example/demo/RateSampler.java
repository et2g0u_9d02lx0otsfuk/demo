package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class RateSampler {

    private final RateService rateService;

    @Value("fixer_token")
    String fixerToken;

    @Value("base_currency")
    String baseCurrency;

    @Value("target_currency")
    String targetCurrency;

    @Value("sampleRateSeconds")
    Long sampleRateSeconds;

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    @Autowired
    public RateSampler(RateService rateService) {
        this.rateService = rateService;
    }

    @PostConstruct
    private void scheduleCheck() {
        scheduler.scheduleAtFixedRate(this::checkRate, sampleRateSeconds, sampleRateSeconds, TimeUnit.SECONDS);
    }

    @PreDestroy
    private void stopSchedule() {
        scheduler.shutdown();
    }

    public Rate checkRate() {
        String url = "http://data.fixer.io/api/latest?access_key=" + fixerToken + "&format=1";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<FixerLatestResponse> response = restTemplate.getForEntity(url, FixerLatestResponse.class);
        FixerLatestResponse fixer = response.getBody();
        Double rateValue = fixer.getCurRateMap().get(targetCurrency);
        Long timestamp = fixer.getTimestamp();
        Rate rate = new Rate(baseCurrency, targetCurrency, rateValue, timestamp);
        rateService.updateRate(rate);
        return rate;
    }
}
